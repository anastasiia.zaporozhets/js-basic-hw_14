"use strict"


const btnTheme = document.getElementById("btn-theme");
const themes = {
    default: {
        "--main-bg-color": "#171717",
        "--footer-bg-color": "black",
        "--color-title": "#FFFFFF",
        "--color-grid-text-first": "#FFFFFF"
    },
    green: {
        "--main-bg-color": "#2F4F4F",
        "--footer-bg-color": "#4a7566",
        "--color-title": "#1f2943",
        "--color-grid-text-first": "#0c2840"
    }
}

let currentTheme = localStorage.getItem('theme') || 'default';

function updateTheme(themeName) {
    const theme = themes[themeName];
    Object.keys(theme).forEach(property => {
        document.documentElement.style.setProperty(property, theme[property]);
    });
}

updateTheme(currentTheme);

btnTheme.addEventListener("click", () => {
    currentTheme = (currentTheme === 'default') ? 'green' : 'default';

    localStorage.setItem('theme', currentTheme);

    updateTheme(currentTheme);
});
